package PresentationLayer;

import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.*;

import BusinessLayer.Restaurant;

public class AdministratorView extends JFrame{
	private JLabel actions=new JLabel("Choose an action as Administrator");
	private JLabel box1=new JLabel("Name of a menu item");
	private JLabel box2=new JLabel("Price or menu items");
	private JTextField text1=new JTextField();
	private JTextField text2=new JTextField();
	private JButton viewAll=new JButton("View all menu items");
	private JButton backbtn=new JButton("Back");
	private JButton createbtn=new JButton("Create a menu item");
	private JButton editPricebtn=new JButton("Edit a base product's price");
	private JButton editCompAddbtn=new JButton("Edit a composite product: add a menu item to it");
	private JButton editCompDeletebtn=new JButton("Edit a composite product: delete a menu item from it");
	private JButton deletebtn=new JButton("Delete a menu item");
	public AdministratorView()
	{
		JPanel continut=new JPanel();
		JLabel space=new JLabel(" ");
		continut.add(actions);
		continut.add(space);
		continut.add(box1);
		continut.add(text1);
		continut.add(box2);
		continut.add(text2);
		continut.add(viewAll);
		continut.add(createbtn);	
		continut.add(editPricebtn);
		continut.add(editCompAddbtn);
		continut.add(editCompDeletebtn);
		continut.add(deletebtn);
		continut.add(backbtn);
		this.createbtn.setMaximumSize(getMaximumSize());
		this.editPricebtn.setMaximumSize(getMaximumSize());
		this.editCompAddbtn.setMaximumSize(getMaximumSize());
		this.editCompDeletebtn.setMaximumSize(getMaximumSize());
		this.deletebtn.setMaximumSize(getMaximumSize());
		this.backbtn.setMaximumSize(getMaximumSize());
		this.viewAll.setMaximumSize(getMaximumSize());
		continut.setLayout(new BoxLayout(continut,BoxLayout.Y_AXIS));
    	this.setContentPane(continut);
    	this.setSize(1000,500);
    	this.setTitle("Administrator Interface");
    	this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
	}
	public void addCreateListener(ActionListener create)
	{
		this.createbtn.addActionListener(create);
	}
	public void addEditPriceListener(ActionListener edit)
	{
		this.editPricebtn.addActionListener(edit);
	}
	public void addEditCompAddListener(ActionListener edit)
	{
		this.editCompAddbtn.addActionListener(edit);
	}
	public void addEditCompDeleteListener(ActionListener edit)
	{
		this.editCompDeletebtn.addActionListener(edit);
	}
	public void addDeleteListener(ActionListener delete)
	{
		this.deletebtn.addActionListener(delete);
	}
	public void addBackListener(ActionListener backl)
	{
		this.backbtn.addActionListener(backl);
	}
	public void addViewAllListener(ActionListener all)
	{
		this.viewAll.addActionListener(all);
	}
	public String getNume()
	{
		return this.text1.getText();
	}
	public String getPM()
	{
		return this.text2.getText();
	}
	public void setTable(JTable t)
	{
		JFrame frame=new JFrame();
		JScrollPane sp=new JScrollPane(t);
		frame.add(sp);
		frame.setSize(500,300);
		frame.setVisible(true);
	}
}
