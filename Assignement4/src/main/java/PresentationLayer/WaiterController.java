package PresentationLayer;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import BusinessLayer.Restaurant;

public class WaiterController {
	private WaiterView view;
    private Restaurant restaurant;
    private String ser;
    
    public WaiterController(WaiterView view,Restaurant restaurant,String ser)
    {
    	this.view=view;
    	this.restaurant=restaurant;
    	this.ser=ser;
    	view.addAllOrdersListener(new ViewAllOrdersListener());
    	view.addCreateOrderListener(new CreateOrderListener());
    	view.addComputeBillListener(new ComputeBillListener());
    	view.addBackOrderListener(new BackOrderListener());
    	view.setVisible(true);
    }
    public class CreateOrderListener implements ActionListener
    {
		@Override
		public void actionPerformed(ActionEvent e) {
		      restaurant.createOrder(view.getOrderId().trim(),view.getDate().trim(),view.getTable().trim(),view.getItems().trim(), ser);
		}
    }
    public class ViewAllOrdersListener implements ActionListener
    {
		@Override
		public void actionPerformed(ActionEvent e) {
		      view.setTable(restaurant.creareTabelOrder());
		}
    }
    public class ComputeBillListener implements ActionListener
    {
		@Override
		public void actionPerformed(ActionEvent e) {
		     restaurant.generatingABill(view.getOrderId().trim(),view.getDate().trim(),view.getTable().trim()); 
		}
    }
    public class BackOrderListener implements ActionListener
    {
		@Override
		public void actionPerformed(ActionEvent e) {
			view.setVisible(false);
			StartView startView=new StartView();
			StartController startController =new StartController(startView,ser); 
		}
    }
}
