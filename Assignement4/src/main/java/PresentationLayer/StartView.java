package PresentationLayer;

import java.awt.FlowLayout;
import java.awt.LayoutManager;
import java.awt.event.ActionListener;

import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;

public class StartView extends JFrame{
    private JButton administratorbtn=new JButton("Administrator");
    private JButton waiterbtn=new JButton("Waiter");
    private JLabel label=new JLabel("Choose your type:");
    public StartView()
    {
    	JPanel continut=new JPanel();
    	JLabel space=new JLabel(" ");
    	continut.add(label);
    	continut.add(space);
    	continut.add(this.administratorbtn);
    	continut.add(this.waiterbtn);
    	continut.setLayout(new BoxLayout(continut,BoxLayout.Y_AXIS));
    	this.administratorbtn.setMaximumSize(getMaximumSize());
    	this.waiterbtn.setMaximumSize(getMaximumSize());
    	this.setContentPane(continut);
    	this.setSize(500,300);
    	this.setTitle("START");
    	this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
    	
    }
    public void addAdministratorListener(ActionListener admin)
    {
        this.administratorbtn.addActionListener(admin);	
    }
    public void addWaiterListener(ActionListener waiter)
    {
    	this.waiterbtn.addActionListener(waiter);
    }
}
