package PresentationLayer;

import java.awt.event.ActionListener;

import javax.swing.*;

public class WaiterView extends JFrame{
	private JButton createbtn=new JButton("Create an order");
	private JButton computeBillbtn=new JButton("Compute the bill for an order");
	private JButton viewAllbtn=new JButton("View all orders");
	private JButton backbtn=new JButton("Back");
	private JLabel label=new JLabel("Choose an action as waiter");
	private JTextField text1=new JTextField();
	private JTextField text2=new JTextField();
	private JTextField text3=new JTextField();
	private JTextField text4=new JTextField();
	private JLabel box1=new JLabel("Order ID");
	private JLabel box2=new JLabel("Date");
	private JLabel box3=new JLabel("Table");
	private JLabel box4=new JLabel("Menu Items for order (just for creating an order)");
	
	public WaiterView()
	{
		JPanel continut=new JPanel();
		JLabel space=new JLabel(" ");
		continut.add(label);
		continut.add(space);
		continut.add(box1);
		continut.add(text1);
		continut.add(box2);
		continut.add(text2);
		continut.add(box3);
		continut.add(text3);
		continut.add(box4);
		continut.add(text4);
		continut.add(viewAllbtn);
		continut.add(createbtn);
		continut.add(computeBillbtn);
		continut.add(backbtn);
		this.viewAllbtn.setMaximumSize(getMaximumSize());
		this.createbtn.setMaximumSize(getMaximumSize());
		this.computeBillbtn.setMaximumSize(getMaximumSize());
		this.backbtn.setMaximumSize(getMaximumSize());
		continut.setLayout(new BoxLayout(continut,BoxLayout.Y_AXIS));
    	this.setContentPane(continut);
    	this.setSize(1000,500);
    	this.setTitle("Waiter Interface");
    	this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
	}
	public void addAllOrdersListener(ActionListener all)
	{
		this.viewAllbtn.addActionListener(all);
	}
	public void addCreateOrderListener(ActionListener create)
	{
		this.createbtn.addActionListener(create);
	}
	public void addComputeBillListener(ActionListener bill)
	{
		this.computeBillbtn.addActionListener(bill);
	}
	public void addBackOrderListener(ActionListener back)
	{
		this.backbtn.addActionListener(back);
	}
	public String getOrderId()
	{
		return text1.getText();
	}
	public String getDate()
	{
		return text2.getText();
	}
	public String getTable()
	{
		return text3.getText();
	}
	public String getItems()
	{
		return text4.getText();
	}
	public void setTable(JTable table)
	{
		JScrollPane sp=new JScrollPane(table);
		JFrame frame=new JFrame();
		frame.add(sp);
		frame.setSize(800,300);
		frame.setVisible(true);
		
	}

}
