package PresentationLayer;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import BusinessLayer.Restaurant;

public class StartController {
      StartView view;
      String ser;
      Restaurant restaurant;
      public StartController(StartView start,String ser)
      {
    	  this.view=start;
    	  this.ser=ser;
    	  this.restaurant=new Restaurant(ser);
    	  view.addAdministratorListener(new AdministratorListener());
    	  view.addWaiterListener(new WaiterListener());
    	  view.setVisible(true);
      }
      class AdministratorListener implements ActionListener
      {
    	  @Override
    		public void actionPerformed(ActionEvent arg0) {
    			AdministratorView adminView=new AdministratorView();
    			AdministratorController adminController=new AdministratorController(restaurant,adminView,ser);
    			view.setVisible(false);
    			
    		}
      }
      class WaiterListener implements ActionListener
      {
    	  @Override
    		public void actionPerformed(ActionEvent arg0) {
    			WaiterView waiterView=new WaiterView();
    			WaiterController waiterController=new WaiterController(waiterView,restaurant,ser);
    			view.setVisible(false);
    		}
      }
}
	