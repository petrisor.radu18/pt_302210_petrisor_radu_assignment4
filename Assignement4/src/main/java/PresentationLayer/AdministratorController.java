package PresentationLayer;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import BusinessLayer.Restaurant;

public class AdministratorController {
	private Restaurant restaurant;
	private AdministratorView view;
	private String ser;
	public AdministratorController(Restaurant r, AdministratorView view,String ser)
	{
		this.restaurant=r;
		this.view=view;
		this.ser=ser;
		view.addCreateListener(new CreateListener());
		view.addEditPriceListener(new EditPriceListener());
		view.addEditCompAddListener(new EditCompAddListener());
		view.addEditCompDeleteListener(new EditCompDeleteListener());
		view.addDeleteListener(new DeleteListener());
		view.addBackListener(new BackListener());
		view.addViewAllListener(new AllListener());
		view.setVisible(true);
	}
    public class CreateListener implements ActionListener
    {
		@Override
		public void actionPerformed(ActionEvent arg0) {
			restaurant.addMenuItemToMeniu(view.getNume().trim(),view.getPM().trim(), ser);
		}	
    }
    public class EditPriceListener implements ActionListener
    {
		@Override
		public void actionPerformed(ActionEvent e) {
			restaurant.editMenuItems(view.getNume().trim(),"base","",view.getPM().trim(), ser);
		}
    }
    public class EditCompAddListener implements ActionListener
    {
		@Override
		public void actionPerformed(ActionEvent e) {
	       restaurant.editMenuItems(view.getNume().trim(),"product","add",view.getPM().trim(), ser);
		}
    }
    public class EditCompDeleteListener implements ActionListener
    {
		@Override
		public void actionPerformed(ActionEvent e) {
			restaurant.editMenuItems(view.getNume().trim(),"product","delete",view.getPM().trim(), ser);	
		}
    }
    public class DeleteListener implements ActionListener
    {
		@Override
		public void actionPerformed(ActionEvent e) {
			restaurant.deleteMenuItemFromMeniu(view.getNume().trim(), ser);	
		}
    }
    public class BackListener implements ActionListener
    {
		@Override
		public void actionPerformed(ActionEvent e) {
			view.setVisible(false);
			StartView startView=new StartView();
			StartController startController =new StartController(startView,ser);
		}	
    }
    public class AllListener implements ActionListener
    {
    	@Override
		public void actionPerformed(ActionEvent e) {
			view.setTable(restaurant.creareTabelMenuItems());
		}	
    }
}
