package DataLayer;

import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectOutputStream;

import BusinessLayer.Restaurant;

public class RestaurantSerializator {
    public void serialization(Restaurant r,String ser)
    {
    	FileOutputStream fileOut;
		try {
			fileOut = new FileOutputStream(ser);
			ObjectOutputStream out = new ObjectOutputStream(fileOut);
			out.writeObject(r);
			out.close();
			fileOut.close();
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
    }
}
