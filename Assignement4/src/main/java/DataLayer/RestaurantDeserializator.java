package DataLayer;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.ObjectInputStream;

import BusinessLayer.Restaurant;

public class RestaurantDeserializator {
  
	public Restaurant deserialization(Restaurant r,String ser)
	{
		try {
	         FileInputStream fileIn = new FileInputStream(ser);
	         ObjectInputStream in = new ObjectInputStream(fileIn);
	         r= (Restaurant) in.readObject();
	         //this.meniu=r.getMenuItems();
	         //this.comenzi=r.getComenzi();
	         in.close();
	         fileIn.close();
	         return r;
	      } catch (IOException i) {
	         i.printStackTrace();
	         return null;
	      } catch (ClassNotFoundException c) {
	         c.printStackTrace();
	         return null;
	      }
	}
}
