package BusinessLayer;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
public class Waiter implements IRestaurantProcessingWaiter,Serializable{
    private String nume;
    public Waiter(String n)
    {
    	this.nume=n;
    }
	@Override
	public HashMap<Order,ArrayList<MenuItem>> createNewOrder(String id,String data,String table,String products,ArrayList<MenuItem> meniu,HashMap<Order,ArrayList<MenuItem>> comenzi) {
		int ordid=Integer.valueOf(id);
		int masa=Integer.valueOf(table);
		Order o=new Order(ordid,data,masa);
		ArrayList<MenuItem> orderItems=new ArrayList<MenuItem>();
		String delimiters="[,]";
		String[] prods;
		prods=products.split(delimiters);
		for(MenuItem m:meniu)
		{
			for(int i=0;i<prods.length;i++)
			if(m.getNume().equals(prods[i].trim()))
			{
				orderItems.add(m);
			}
		}
		comenzi.put(o, orderItems);
		return comenzi;
	}
	@Override
	public float computePriceForOrder(ArrayList<MenuItem> o) {
		float suma=0;
		for(MenuItem item:o)
		{
			suma=suma+item.computePrice();
		}
		return suma;
	}
	@Override
	public void generateBill(ArrayList<MenuItem> m,Order o) {
		String s=Integer.toString(o.getOrderID());
		String s1="BillNumber".concat(s);
		String s2=s1.concat(".txt");
		File file=new File(s2);
		try {
			FileWriter writer=new FileWriter(file);
            float sum=0;
            writer.write(o.toString()+"\n");
			for(MenuItem m1:m)
			{
			writer.write("Ordered: "+m1.toString()+"\n");
			}
			sum=computePriceForOrder(m);
			writer.write("Total price: "+sum);
			writer.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
		
	}
	

}
