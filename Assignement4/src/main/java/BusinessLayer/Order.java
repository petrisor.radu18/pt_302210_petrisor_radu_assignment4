package BusinessLayer;

import java.io.Serializable;

public class Order implements Serializable{
     private int OrderID;
     private String data;
     private int table;
     public Order(int id, String d,int t)
     {
    	 this.OrderID=id;
    	 this.data=d;
    	 this.table=t;
     }
     public int getOrderID()
     {
    	 return this.OrderID;
     }
     public String getData()
     {
    	 return this.data;
     }
     public int getTable()
     {
    	 return this.table;
     }
     public void setOrderID(int nr)
     {
    	 this.OrderID=nr;
     }
     public void setData(String data)
     {
    	 this.data=data;
     }
     public void setTable(int t)
     {
    	this.table=t;
     }
     public int hashCode()
     {
		return this.OrderID;
    	 
     }
     public boolean equals(Object obj)
     {
    	 return this.OrderID==((Order)obj).getOrderID();
     }
     public String toString()
     {
    	 return "Order "+this.OrderID+" has been succesfully created on "+this.data+" for table "+this.table;
     }
}
