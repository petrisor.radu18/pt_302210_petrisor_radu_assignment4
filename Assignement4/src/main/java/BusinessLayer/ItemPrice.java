package BusinessLayer;

public interface ItemPrice {
	public float computePrice();
}
