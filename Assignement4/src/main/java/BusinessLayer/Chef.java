package BusinessLayer;

import java.io.Serializable;
import java.util.Observable;
import java.util.Observer;

import javax.swing.JFrame;
import javax.swing.JLabel;

public class Chef implements Serializable,Observer{
	private String nume;
	public Chef(String n)
	{
		this.nume=n;
	}
	@Override
	public void update(Observable restaurant, Object preparat) {
		String preparatChef=(String)preparat;
		System.out.println("The chef will prepare "+preparatChef);
	}
}
