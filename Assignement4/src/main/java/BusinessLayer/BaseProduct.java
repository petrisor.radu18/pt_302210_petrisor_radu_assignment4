package BusinessLayer;

import java.io.Serializable;

public class BaseProduct extends MenuItem implements ItemPrice,Serializable{
	public BaseProduct(String n,float p)
	{
		super(n,p);
	}
	@Override
	public float computePrice() {
		return super.pret;
	}
}
