package BusinessLayer;

import java.io.Serializable;
import java.util.ArrayList;

public class CompositeProduct extends MenuItem implements ItemPrice,Serializable{
     private ArrayList<MenuItem> ingrediente = new ArrayList<MenuItem>();  
     public CompositeProduct(String n)
     {
    	super(n,0);
     }
     public void addProduct(String produs, ArrayList<MenuItem> meniu)
     {
    	 for(MenuItem m:meniu)
    	 {
    		 if(produs.equals(m.getNume()))
    	       {
    			 ingrediente.add(m);
    			 break;
    	       }
    	 }
    	 float pretTotal=this.computePrice();
    	 super.setPret(pretTotal);
    	
     }
     public void deleteProduct(String produs)
     {
    	 int index=0;
    	 for(MenuItem p:ingrediente)
    	 {
    		 if(p.getNume().equals(produs)==true)
    		 {
    	    	 break;
    		 }
    		index++;
    	 }
    	 ingrediente.remove(index);
    	 float pretTotal=this.computePrice();
    	 super.setPret(pretTotal);
     }
	@Override
	public float computePrice() {
		float pretTotal=0;
		for(MenuItem b:this.ingrediente)
		{
			pretTotal=pretTotal+b.computePrice();
		}
		return pretTotal;
	}

	
}
