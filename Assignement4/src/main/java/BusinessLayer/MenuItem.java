package BusinessLayer;

import java.io.Serializable;
import java.util.ArrayList;

public class MenuItem implements ItemPrice,Serializable{
	protected String nume;
	protected float pret;
    public MenuItem(String n,float p)
    {
    	this.nume=n;
    	this.pret=p;
    }
    public String getNume()
	{
		return this.nume;
	}
    public void setNume(String s)
    {
    	this.nume=s;
    }
    public void setPret(float p)
    {
    	this.pret=p;
    }
	@Override
	public float computePrice() {
		return this.pret;
	}
	public String toString()
	{
		return this.nume+" "+this.pret;
	}
}
