package BusinessLayer;

import java.util.ArrayList;
/**
 *@inv meniu!=null
 */
public interface IRestaurantProcessingAdministrator {
    public MenuItem createMenuItem(String nume,String pret,String tip,ArrayList<MenuItem> m);
    public  ArrayList<MenuItem>  deleteMenuItem(ArrayList<MenuItem> meniu,String m);
    public ArrayList<MenuItem> editMenuItem(String nume,String tip,String actiune,String var,ArrayList<MenuItem> meniu);
}
