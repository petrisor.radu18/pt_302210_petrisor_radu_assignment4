package BusinessLayer;

import java.awt.print.PrinterException;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.text.MessageFormat;
import java.util.ArrayList;
//import com.google.java.contract.*;
import java.util.HashMap;
import java.util.Observable;

import javax.swing.JFrame;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.table.DefaultTableModel;

import DataLayer.RestaurantDeserializator;
import DataLayer.RestaurantSerializator;
/**
 *  Restaurant must have at least one item in the menu otherwise he can't sell nothing
 *  @inv meniu!=null 
 *  @pre ser!=null in all methods
 */
public class Restaurant extends Observable implements Serializable{
	ArrayList<MenuItem> meniu=new ArrayList<MenuItem>();
	HashMap<Order,ArrayList<MenuItem>> comenzi=new HashMap<>();
	Administrator admin=new Administrator("Administrator");
	Waiter waiter=new Waiter("Waiter");
	Chef chef=new Chef("Chef");
	transient RestaurantDeserializator restaurantDes=new RestaurantDeserializator();
	transient RestaurantSerializator restaurantSer=new RestaurantSerializator();
	static final long serialVersionUID=1L;
	/**
	 * 
	 * @pre ser!=null
	 */
	public Restaurant(String ser)
	{
		this.addObserver(chef);
		Restaurant r=restaurantDes.deserialization(this, ser);
		 this.meniu=r.getMenuItems();
         this.comenzi=r.getComenzi();
	}
	/**
	 * @pre e!=null
	 * @post meniu.size()=meniu@pre.size()+1
	 * @post meniu.contains(e)
	 */
	public void addMenuItem(MenuItem e,String ser)
	{
		int dimensiune=meniu.size();
		assert e!=null;
		meniu.add(e);
		assert dimensiune+1==meniu.size();
		assert meniu.contains(e);
		restaurantSerialization(ser);
	}
	/**
	 * 
	 * @post The table will have all the menu items and their attributes
	 */
	public JTable creareTabelMenuItems()
	{
		DefaultTableModel model=new DefaultTableModel();
		model.addColumn("Nume");
		model.addColumn("Pret");
		for(MenuItem m:meniu)
		{
			String []v= {m.getNume(),String.valueOf(m.computePrice())};
			model.addRow(v);
		}
		JTable table=new JTable(model);
		return table;
	}
	/**
	 * 
	 * @post The table will have all the orders and the items ordered
	 */
	public JTable creareTabelOrder()
	{
		DefaultTableModel model=new DefaultTableModel();
		model.addColumn("ID");
		model.addColumn("Data");
		model.addColumn("Table");
		model.addColumn("Items Ordered");
		for(Order o:comenzi.keySet())
		{
			ArrayList<MenuItem> orderMenu=comenzi.get(o);
			StringBuilder items=new StringBuilder();
			for(MenuItem m:orderMenu)
			{
				items.append(m.getNume());
				items.append(",");
			}
			int size=items.length();
			items.deleteCharAt(size-1);
			String []v= {String.valueOf(o.getOrderID()),o.getData(),String.valueOf(o.getTable()),items.toString()};
			model.addRow(v);
		}
		JTable table=new JTable(model);
		return table;
	}
	/**
	 * @pre id!=null && integer type
	 * @pre data!=null
	 * @pre table!=null && integer type
	 * @pre products!=null
	 */
	public void createOrder(String id,String data,String table,String products,String ser)
	{
		assert id!=null; assert data!=null; assert table!=null; assert products!=null;
		assert id.matches("\\d+"); assert table.matches("\\d+");
		String []comanda;
		String delimitator="[,]";
		comanda=products.split(delimitator);
		setComenzi(waiter.createNewOrder(id, data, table, products, meniu,comenzi));
		restaurantSerialization(ser);
		for(MenuItem m:meniu)
		{
			for(int i=0;i<comanda.length;i++)
			{
				if(m.getNume().equals(comanda[i].trim()))
				{
					if(m instanceof CompositeProduct)
					{
						setChanged();
						notifyObservers(comanda[i].trim());
					}
				}
			}
		}
	}
	/**
	 * @pre id!=null && integer type
	 * @pre data!=null
	 * @pre table!=null && integer type
	 * @post price>0
	 */
	public  float getPriceForOrder(String id,String data,String table)
	{
		assert id!=null; assert data!=null; assert table!=null;
		assert id.matches("\\d+"); assert table.matches("\\d+");
		int orderid=Integer.valueOf(id);
		int tablenumber=Integer.valueOf(table);
		Order o=new Order(orderid,data,tablenumber);
		ArrayList<MenuItem> items=new ArrayList<MenuItem>();
		if(comenzi.containsKey(o))
	     items=comenzi.get(o);
		else 
		{
			System.out.println("Nu exista comanda:"+o.getOrderID()+" "+o.getData()+" "+o.getTable());
		}
		float price=waiter.computePriceForOrder(items);
		assert price>0;
		return price;
	}
	/**
	 * @pre nume!=NULL
	 * @pre meniu.contains(m)
	 * @post meniu.size()=meniu@pre.size()-1
	 */
	
	public void deleteMenuItemFromMeniu(String nume,String ser)
	{
		int dimensiune=meniu.size();
		assert nume!=null;
		setMenuItems(admin.deleteMenuItem(getMenuItems(), nume));
		assert dimensiune-1==meniu.size();
		restaurantSerialization(ser);
	}
	/**
	 * @pre nume!=null
	 * @pre var!=null
	 * @post meniu.size()=meniu@pre.size()+1
	 * @post meniu.contains(m)
	 */
	public void addMenuItemToMeniu(String nume,String var,String ser)
	{
		assert nume!=null; assert var!=null; int dimensiune=meniu.size();
		String s=var.trim();
		try
		{
			float pret=Float.parseFloat(var);
			MenuItem m=admin.createMenuItem(nume, var,"base",getMenuItems());
			addMenuItem(m,ser);
			assert dimensiune+1==meniu.size(); assert meniu.contains(m);
			restaurantSerialization(ser);
		}
		catch(NumberFormatException nfe)
		{
			MenuItem m=admin.createMenuItem(nume, var,"compose",getMenuItems());
			addMenuItem(m,ser);
			assert dimensiune+1==meniu.size(); assert meniu.contains(m);
			restaurantSerialization(ser);
		}
	}
	/**
	 * @pre nume
	 * @pre tip
	 * @pre actiune
	 * @pre var
	 */
	public void editMenuItems(String nume,String tip,String actiune,String var,String ser)
	{
		assert nume!=null; assert tip!=null; assert actiune!=null; assert var!=null;
		admin.editMenuItem(nume, tip, actiune, var, meniu);
		restaurantSerialization(ser);
	}
	/**
	 * @pre id!=null && integer type
	 * @pre data!=null
	 * @pre table!=null && integer type
	 */
	public void generatingABill(String id,String data,String table)
	{
		assert id!=null; assert data!=null; assert table!=null;
		assert id.matches("\\d+"); assert table.matches("\\d+");
		int orderid=Integer.valueOf(id);
		int tablenumber=Integer.valueOf(table);
		Order o=new Order(orderid,data,tablenumber);
		waiter.generateBill(comenzi.get(o),o);
	}
	public void restaurantSerialization(String ser)
	{
		this.restaurantSer.serialization(this, ser);		
	}
	public void setMenuItems(ArrayList<MenuItem> m)
	{
		this.meniu=m;
	}
	public ArrayList<MenuItem> getMenuItems()
	{
		return this.meniu;
	}
	public void setComenzi(HashMap<Order,ArrayList<MenuItem>> c)
	{
		this.comenzi=c;
	}
	public HashMap<Order,ArrayList<MenuItem>> getComenzi()
	{
		return this.comenzi;
	}
}
