package BusinessLayer;

import java.io.Serializable;
import java.util.ArrayList;

public class Administrator implements IRestaurantProcessingAdministrator,Serializable{
	private String nume;
	public Administrator(String n)
	{
		this.nume=n;
	}
	@Override
	public MenuItem createMenuItem(String nume,String var,String tip,ArrayList<MenuItem> m) {
		if(tip.equals("base"))
		{
		float p=Float.valueOf(var);
		BaseProduct item=new BaseProduct(nume,p);
		return item;
		}
		else
		{
			CompositeProduct p=new CompositeProduct(nume);
			String delimiters="[,]";
			String[] products;
			products=var.split(delimiters);
			for(MenuItem m1:m)
			{
				for(int i=0;i<products.length;i++)
				{
					if(m1.getNume().equals(products[i].trim()))
					{
						p.addProduct(m1.getNume(),m);
					}
				}
			}
			return p;
		}
	}
	@Override
	public ArrayList<MenuItem> deleteMenuItem(ArrayList<MenuItem> meniu,String m) {
		int index=0;
		for(MenuItem item:meniu)
		{
			if(item.getNume().equals(m))
			{
				break;
			}
				index++;
		}
		meniu.remove(index);
		return meniu;
	}
	@Override
	public ArrayList<MenuItem> editMenuItem(String nume,String tip,String actiune,String var,ArrayList<MenuItem> meniu) {
		String tip1=tip.trim();
		if(tip1.equals("base"))
		{
			float p=Float.valueOf(var);
			for(MenuItem m:meniu)
			{
				if(nume.equals(m.getNume()))
				{
					m.setPret(p);
				}
			}
		}
		else
		{
				for(MenuItem m:meniu)
				{
					if(m instanceof CompositeProduct)
					{
						CompositeProduct c=(CompositeProduct)m;
					if(nume.equals(c.getNume().trim()))
					{
						if(actiune.equals("delete"))
						{
							c.deleteProduct(var);
						}
						else
						{
							c.addProduct(var, meniu);
						}
					}
					}
				}
		}
		return meniu;
	}


}
