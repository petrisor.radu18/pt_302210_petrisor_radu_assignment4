package BusinessLayer;

import java.util.ArrayList;
import java.util.HashMap;
/**
 *@inv meniu!=null
 */
public interface IRestaurantProcessingWaiter {
  public HashMap<Order,ArrayList<MenuItem>> createNewOrder(String id,String data,String table,String products,ArrayList<MenuItem> meniu,HashMap<Order,ArrayList<MenuItem>> comenzi);
  public float computePriceForOrder(ArrayList<MenuItem> o);
  public void generateBill(ArrayList<MenuItem> m,Order o);
}
